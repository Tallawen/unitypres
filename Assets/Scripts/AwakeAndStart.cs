﻿using UnityEngine;
using System.Collections;

public class AwakeAndStart : MonoBehaviour {
	void Awake() {
		Debug.Log("Awake");
	}
	
	void Start() {
		Debug.Log("Start");
	}

	void OnEnabled() { 
		Debug.Log("OnEnabled");
	}

	void OnDisable() {
		Debug.Log("OnDisable");
	}

	void OnDestroy() {
		Debug.Log("OnDestroy");
	}
}