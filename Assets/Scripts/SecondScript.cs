﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondScript : MonoBehaviour {

	public bool someValue;

	public void someFunction() 
	{
		Debug.Log(string.Format("SomeFunction: {0}", someValue.ToString()));		
	}
}
