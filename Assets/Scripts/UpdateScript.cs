﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateScript : MonoBehaviour {

	public GameObject Object;
	
	public bool Fixed = false;

	public Vector3 Directions = new Vector3(0, 0, -1);

	[Range(0, 10)]
	public float Speed = 0;

	void Update () {
		if(!Fixed)
			Object.transform.position = Vector3.Lerp(Object.transform.position, Object.transform.position + Directions, Time.deltaTime * Speed);
	}

	void FixedUpdate() {
		if(Fixed)
			Object.transform.position = Vector3.Lerp(Object.transform.position, Object.transform.position + Directions, Time.deltaTime * Speed);
	}
}
