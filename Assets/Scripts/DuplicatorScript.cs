﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class DuplicatorScript : MonoBehaviour {

	public GameObject Prefab;

	public GameObject Parent;

	[Range(0, 40)]
	public int Count = 1;

	public Vector3 StartPosition = new Vector3(0, 0, 0);
	public Vector3 Offset = new Vector3(0, 0, 0);
	
	private List<GameObject> _clones = new List<GameObject>();
	
	void Update() {
		foreach(var item in _clones)
			GameObject.DestroyImmediate(item);

		_clones.Clear();

		if(Prefab != null) {
			for(int i = 0; i < Count; ++i) {
				Vector3 pos = StartPosition + Offset * i;

				GameObject clone = Instantiate(Prefab, pos, transform.rotation);

				_clones.Add(clone);

				if(Parent != null)
					clone.transform.parent = Parent.transform;
			}
		}
	}
}
