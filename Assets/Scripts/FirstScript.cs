﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstScript : MonoBehaviour {

	public GameObject partner;

	void Start () {
		//SecondScript sc = gameObject.GetComponentInChildren<SecondScript>();
		//sc.someValue = false;
		//sc.someFunction();

		//SecondScript sc = partner.GetComponent<SecondScript>();
		//sc.someValue = false;
		//sc.someFunction();

		GameObject obj = GameObject.Find("Floor");
		SecondScript sc = obj.GetComponent<SecondScript>();
		sc.someValue = false;
		sc.someFunction();
	}
}
