﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public bool Fixed = false;

	public GameObject Object;

	private Vector3 _offset;

	void Start() {
		_offset = transform.position - Object.transform.position;
	}

	void LateUpdate() {
		if(!Fixed)
			transform.position = Object.transform.position + _offset;
	}

	void FixedUpdate() {
		if(Fixed)
			transform.position = Object.transform.position + _offset;
	}
}